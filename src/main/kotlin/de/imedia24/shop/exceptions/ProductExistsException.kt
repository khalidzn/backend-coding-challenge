package de.imedia24.shop.exceptions

class ProductExistsException(message: String) : RuntimeException(message)