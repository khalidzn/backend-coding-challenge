package de.imedia24.shop.exceptions

class ProductNotFoundException(message: String) : RuntimeException(message)