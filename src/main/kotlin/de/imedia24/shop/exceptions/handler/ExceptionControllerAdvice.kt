package de.imedia24.shop.exceptions.handler

import de.imedia24.shop.dto.ErrorMessageModel
import de.imedia24.shop.exceptions.ProductExistsException
import de.imedia24.shop.exceptions.ProductNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionControllerAdvice {

    @ExceptionHandler
    fun handleValidatationException(ex: MethodArgumentNotValidException): ResponseEntity<ErrorMessageModel> {

        var error = ""
        if(ex.hasErrors()){
            error = ex.allErrors.map { err -> err.defaultMessage }.joinToString(" \n")
        }
        val errorMessage = ErrorMessageModel(
            HttpStatus.BAD_REQUEST.value(),
            error
        )
        return ResponseEntity(errorMessage, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler
    fun handleProductExistsException(ex: ProductExistsException): ResponseEntity<ErrorMessageModel> {

        val errorMessage = ErrorMessageModel(
            HttpStatus.BAD_REQUEST.value(),
            ex.message
        )
        return ResponseEntity(errorMessage, HttpStatus.BAD_REQUEST)
    }

    @ExceptionHandler
    fun handleProductNotFoundException(ex: ProductNotFoundException): ResponseEntity<ErrorMessageModel> {

        val errorMessage = ErrorMessageModel(
            HttpStatus.NOT_FOUND.value(),
            ex.message
        )
        return ResponseEntity(errorMessage, HttpStatus.NOT_FOUND)
    }
}