package de.imedia24.shop.service

import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.dto.ProductDto
import de.imedia24.shop.dto.ProductDto.Companion.toNewProductEntity
import de.imedia24.shop.dto.ProductDto.Companion.toProductResponse
import de.imedia24.shop.dto.SimplifiedProductDto
import de.imedia24.shop.dto.SimplifiedProductDto.Companion.toUpdatableProductEntity
import de.imedia24.shop.exceptions.ProductExistsException
import de.imedia24.shop.exceptions.ProductNotFoundException
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository) {

    fun findProductBySku(sku: String): ProductDto? {
        return productRepository.findBySku(sku)?.toProductResponse()
    }

    fun findProductsBySkus(skus: List<String>): List<ProductDto>? {
        return productRepository.findBySkuIn(skus)?.map {
                p ->p.toProductResponse()
        }
    }

    fun addProduct(product: ProductDto): ProductDto? {
        val productWithSameSku = productRepository.findByIdOrNull(product.sku)
        return if(productWithSameSku == null){
            val savedProduct = productRepository.save(product.toNewProductEntity())
            savedProduct.toProductResponse()
        } else {
            throw ProductExistsException("Product with sku : ${product.sku} already exists")
        }
    }

    fun updateProduct(sku: String, product: SimplifiedProductDto): ProductDto? {
        val existingProduct = productRepository.findByIdOrNull(sku)
        return if(existingProduct != null){
            val updatedProduct = productRepository.save(product.toUpdatableProductEntity(existingProduct))
            updatedProduct.toProductResponse()
        } else {
            throw ProductNotFoundException("Product with sku : ${sku} not found")
        }
    }

}
