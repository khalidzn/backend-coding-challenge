package de.imedia24.shop.controller

import de.imedia24.shop.dto.ProductDto
import de.imedia24.shop.dto.SimplifiedProductDto
import de.imedia24.shop.service.ProductService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @Operation(summary = "Get product by SKU")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "OK product found"),
        ApiResponse(responseCode = "404", description = "Product not found")
    ])
    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductDto> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)
        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @Operation(summary = "Get products by SKUs")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "OK products found"),
        ApiResponse(responseCode = "404", description = "Products not found")
    ])
    @GetMapping("/products", produces = ["application/json;charset=utf-8"])
    fun findProductsBySkus(
        @RequestParam("skus") skus: List<String>
    ): ResponseEntity<List<ProductDto>> {
        logger.info("Request for products $skus")

        val products = productService.findProductsBySkus(skus)
        if(products != null && products.isNotEmpty()){
            return ResponseEntity.ok(products)
        }
        return ResponseEntity.notFound().build()
    }

    @Operation(summary = "Add product")
    @ApiResponses(value = [
        ApiResponse(responseCode = "201", description = "Product created"),
        ApiResponse(responseCode = "400", description = "Bad request")
    ])
    @PostMapping("/products", produces = ["application/json;charset=utf-8"])
    fun addProduct(@Valid @RequestBody product: ProductDto) : ResponseEntity<ProductDto>{
        logger.info("Adding new product : $product")

        val savedProduct = productService.addProduct(product)
        if(savedProduct != null){
            return ResponseEntity(savedProduct, HttpStatus.CREATED)
        }
        return ResponseEntity<ProductDto>(HttpStatus.BAD_REQUEST)
    }

    @Operation(summary = "Update product")
    @ApiResponses(value = [
        ApiResponse(responseCode = "200", description = "OK product updated"),
        ApiResponse(responseCode = "404", description = "Product not found")
    ])
    @PutMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun updateProduct(@Valid @RequestBody product: SimplifiedProductDto,
                      @PathVariable("sku") sku: String): ResponseEntity<ProductDto>{
        logger.info("Updating product : $product")

        val updatedProduct = productService.updateProduct(sku, product)
        if(updatedProduct != null){
            return ResponseEntity.ok(updatedProduct)
        }
        return ResponseEntity.notFound().build()
    }
}
