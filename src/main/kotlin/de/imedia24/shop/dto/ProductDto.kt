package de.imedia24.shop.dto

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.validation.constraints.Pattern

data class ProductDto(
    @field:Pattern(regexp="[\\d]{6}")
    val sku: String,
    val name: String,
    val description: String,
    val price: BigDecimal,
    val stock: Int
) {
    companion object {
        fun ProductEntity.toProductResponse() = ProductDto(
            sku = sku,
            name = name,
            description = description ?: "",
            price = price,
            stock = stock
        )

        fun ProductDto.toNewProductEntity() = ProductEntity(
            sku = sku,
            name = name,
            description = description,
            price = price,
            stock = stock,
            createdAt = ZonedDateTime.now(),
            updatedAt = ZonedDateTime.now()
        )

        fun ProductDto.toUpdatableProductEntity(existingProduct: ProductEntity) = ProductEntity(
            sku = existingProduct.sku,
            name = name,
            description = description,
            price = price,
            stock = existingProduct.stock,
            createdAt = existingProduct.createdAt,
            updatedAt = ZonedDateTime.now()
        )
    }
}
