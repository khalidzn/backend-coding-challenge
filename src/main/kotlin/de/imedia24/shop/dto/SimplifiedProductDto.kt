package de.imedia24.shop.dto

import de.imedia24.shop.db.entity.ProductEntity
import java.math.BigDecimal
import java.time.ZonedDateTime

data class SimplifiedProductDto(
    val name: String?,
    val description: String?,
    val price: BigDecimal?,
){
    companion object {
        fun SimplifiedProductDto.toUpdatableProductEntity(existingProduct: ProductEntity) = ProductEntity(
            sku = existingProduct.sku,
            name = name?: existingProduct.name,
            description = description,
            price = price?: existingProduct.price,
            stock = existingProduct.stock,
            createdAt = existingProduct.createdAt,
            updatedAt = ZonedDateTime.now()
        )
    }
}