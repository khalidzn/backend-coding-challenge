package de.imedia24.shop.dto

class ErrorMessageModel(
    var status: Int? = null,
    var message: String? = null
)