package de.imedia24.shop.controller

import com.fasterxml.jackson.databind.ObjectMapper
import de.imedia24.shop.dto.ProductDto
import de.imedia24.shop.dto.SimplifiedProductDto
import de.imedia24.shop.service.ProductService
import org.hamcrest.Matchers.`is`
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.math.BigDecimal

@WebMvcTest(ProductController::class)
class ProductControllerTest {

    @MockBean
    lateinit var productService: ProductService


    @Autowired
    lateinit var mockMvc: MockMvc


    @Test
    fun `test findProductsBySku with existing sku`() {
        val sku = "12345"
        val productDto = ProductDto(sku, "Test Product", "Description", BigDecimal.TEN, 1)
        `when`(productService.findProductBySku(sku)).thenReturn(productDto)

        mockMvc.perform(get("/products/$sku"))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.sku", `is`(sku)))
            .andExpect(jsonPath("$.name", `is`("Test Product")))
            .andExpect(jsonPath("$.price", `is`(10)))
    }

    @Test
    fun `test findProductsBySku with non-existing sku`() {
        val sku = "invalid_sku"
        `when`(productService.findProductBySku(sku)).thenReturn(null)

        mockMvc.perform(get("/products/$sku"))
            .andExpect(status().isNotFound)
    }

    @Test
    fun `test addProduct - success`() {
        val SKU = "123456"
        val productDto = ProductDto(SKU, "Test Product", "Description", BigDecimal.TEN, 1)
        `when`(productService.addProduct(productDto)).thenReturn(productDto)

        mockMvc.perform(
            post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(productDto)))
            .andExpect(status().isCreated)
            .andExpect(jsonPath("$.sku", `is`(SKU)))
            .andExpect(jsonPath("$.name", `is`("Test Product")))
            .andExpect(jsonPath("$.price", `is`(10)))
    }


    @Test
    fun `test addProduct - bad request as sku is not valid`() {
        val SKU = "invalid_sku"
        val productDto = ProductDto(SKU, "Test Product", "Description", BigDecimal.TEN, 1)
        `when`(productService.addProduct(productDto)).thenReturn(productDto)

        mockMvc.perform(
            post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectMapper().writeValueAsString(productDto)))
            .andExpect(status().isBadRequest)
    }


    @Test
    fun `test updateProduct with existing product`() {
        val SKU = "12345"
        val simplifiedProductDto = SimplifiedProductDto("Product name updated", "Description updated", BigDecimal.TEN)
        `when`(productService.updateProduct(SKU, simplifiedProductDto)).thenReturn(ProductDto(SKU, "Product name updated", "Description", BigDecimal.TEN, 1))

        mockMvc.perform(
            put("/products/${SKU}")
            .contentType(MediaType.APPLICATION_JSON)
            .content(ObjectMapper().writeValueAsString(simplifiedProductDto)))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.sku", `is`(SKU)))
            .andExpect(jsonPath("$.name", `is`("Product name updated")))
            .andExpect(jsonPath("$.price", `is`(10)))
    }

    @Test
    fun `test updateProduct with non-existing product`() {
        val simplifiedProductDto = SimplifiedProductDto("Product name updated", "Description updated", BigDecimal.TEN)
        `when`(productService.updateProduct("sku", simplifiedProductDto)).thenReturn(null)

        mockMvc.perform(put("/products/sku")
            .contentType(MediaType.APPLICATION_JSON)
            .content(ObjectMapper().writeValueAsString(simplifiedProductDto)))
            .andExpect(status().isNotFound)
    }
}
